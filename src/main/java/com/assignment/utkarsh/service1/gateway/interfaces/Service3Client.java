package com.assignment.utkarsh.service1.gateway.interfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

import com.assignment.utkarsh.service1.models.Name;

@FeignClient(url="ec2-13-234-204-92.ap-south-1.compute.amazonaws.com:8082",name="Service-3")
public interface Service3Client {

	
	@PostMapping("/name")
	public ResponseEntity<String> callByName(Name name);
	
	
	
}
