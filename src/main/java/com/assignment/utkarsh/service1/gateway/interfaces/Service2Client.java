package com.assignment.utkarsh.service1.gateway.interfaces;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(url="ec2-13-234-204-92.ap-south-1.compute.amazonaws.com:8081",name="Service-2")
public interface Service2Client {

	
	@GetMapping("/*")
	public ResponseEntity<String> getRespone();
	
	
	
}
