package com.assignment.utkarsh.service1.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.assignment.utkarsh.service1.mappers.MapName;
import com.assignment.utkarsh.service1.models.Name;
import com.assignment.utkarsh.service1.services.exceptions.APICallException;
import com.assignment.utkarsh.service1.services.interfaces.ISecondApplicationService;
import com.assignment.utkarsh.service1.services.interfaces.IThirdApplicationService;

@RestController
public class ServiceController {

	private static final Logger logger = LoggerFactory.getLogger(ServiceController.class);

	@Autowired
	ISecondApplicationService secondApplicationService;

	@Autowired
	IThirdApplicationService thirdApplicationService;

	@GetMapping("/*")
	public String getServiceStatus() {
		logger.info("Received Get Call to Service 1");
		return "UP";
	}

	@PostMapping("/name")
	public ResponseEntity<String> getResponseFromHelperMs(@RequestBody Name name) {
		String responseFromService2 = null;
		String responseFromService3 = null;
		logger.info("Received Post Call to Service 1  with input-->" + name);
		try {
			responseFromService2 = secondApplicationService.getResponseFromSecondService();

			responseFromService3 = thirdApplicationService.callPostToThirdService(MapName.map(name));

		} catch (APICallException e) {
			logger.info("Response from Service 2-->" + responseFromService2);
			logger.info("Response from Service 3-->" + responseFromService3);
			logger.error(e.getLocalizedMessage(),e.getCause());
			return new ResponseEntity<>(HttpStatus.BAD_GATEWAY);
		} catch (Exception e) {
			logger.info("Response from Service 2-->" + responseFromService2);
			logger.info("Response from Service 3-->" + responseFromService3);
			logger.error(e.getLocalizedMessage(),e.getCause());
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		logger.info("Final Response -->" + responseFromService2 + " " + responseFromService3);
		return new ResponseEntity<String>(responseFromService2 + " " + responseFromService3, HttpStatus.OK);

	}

}
