package com.assignment.utkarsh.service1.services.interfaces.implementations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;

import com.assignment.utkarsh.service1.gateway.interfaces.Service3Client;
import com.assignment.utkarsh.service1.models.Name;
import com.assignment.utkarsh.service1.services.exceptions.APICallException;
import com.assignment.utkarsh.service1.services.interfaces.IThirdApplicationService;

public class ThirdApplicationService implements IThirdApplicationService {

	private static final Logger logger = LoggerFactory.getLogger(SecondApplicationService.class);

	@Autowired
	Service3Client service3Client;
	
	@Override
	public String callPostToThirdService(Name name) throws APICallException {
		
		logger.info("Calling Third Service API  with Input-->"+ name);
		ResponseEntity<String> response = service3Client.callByName(name);
		if (response.getStatusCode().is2xxSuccessful()) {
			String nameResponse =response.getBody();
			logger.info("ThirdApplicationService :Response Received from   Third Service API call->"+ nameResponse);
			return nameResponse;
		}
		else
		{
			throw new APICallException("Error occured from Service 3 or response is null");
		}
	}

}
