package com.assignment.utkarsh.service1.services.exceptions;

public class APICallException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public APICallException(String message) {
		super(message);
	}

	public APICallException() {
	}

	public APICallException(String message, Throwable e) {
		super(message, e);
	}
}
