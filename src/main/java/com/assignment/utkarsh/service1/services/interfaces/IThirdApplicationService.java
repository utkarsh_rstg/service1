package com.assignment.utkarsh.service1.services.interfaces;

import com.assignment.utkarsh.service1.models.Name;

public interface IThirdApplicationService {
	
	public String callPostToThirdService(Name name);

}
