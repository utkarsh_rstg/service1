package com.assignment.utkarsh.service1.services.interfaces.implementations;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.assignment.utkarsh.service1.gateway.interfaces.Service2Client;
import com.assignment.utkarsh.service1.services.exceptions.APICallException;
import com.assignment.utkarsh.service1.services.interfaces.ISecondApplicationService;

public class SecondApplicationService implements ISecondApplicationService {

	private static final Logger logger = LoggerFactory.getLogger(SecondApplicationService.class);

	@Autowired
	Service2Client service2Client;

	@Override
	public String getResponseFromSecondService() throws APICallException {
		logger.info("SecondApplicationService :calling SeconD Service API");
		ResponseEntity<String> response = service2Client.getRespone();
		if (response.getStatusCode().is2xxSuccessful()) {
			return response.getBody();
		} else {
			throw new APICallException("Error Calling  Service 2 or response is null");
		}
	}

}
