package com.assignment.utkarsh.service1.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.assignment.utkarsh.service1.services.interfaces.ISecondApplicationService;
import com.assignment.utkarsh.service1.services.interfaces.IThirdApplicationService;
import com.assignment.utkarsh.service1.services.interfaces.implementations.SecondApplicationService;
import com.assignment.utkarsh.service1.services.interfaces.implementations.ThirdApplicationService;

@Configuration
public class Configurations {

	@Bean
	public ISecondApplicationService getSecondService() {
		return new SecondApplicationService();

	}

	@Bean
	public IThirdApplicationService getThirdService() {
		return new ThirdApplicationService();

	}

}
