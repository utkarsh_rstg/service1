package com.assignment.utkarsh.service1.models;

import org.springframework.lang.NonNull;

public class Name {

	public Name(String name,String surname) {
		this.firstName = name;
		this.surname = surname;
	}

	@NonNull 
	private String firstName;

	@NonNull 
	private String surname;

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	@Override
	public String toString() {
		return firstName + " " + surname;
	}

}
