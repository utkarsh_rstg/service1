FROM openjdk:8
EXPOSE 8080
ADD target/centime-service1.jar centime-service1.jar
ENTRYPOINT ["java","-jar","/centime-service1.jar"]